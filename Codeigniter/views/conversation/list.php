<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Conversations
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
            <li class="active">Contents</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <?php if ($this->session->flashdata('Success')) { ?>
                    <div class="alert alert-success alert-dismissible">
                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                        <h4>
                            <i class="icon fa fa-check"></i>
                            Success!
                        </h4>
                        <?php echo $this->session->flashdata('Success'); ?>
                    </div>
                <?php } ?>

                <?php if (($this->session->flashdata('error') != "")) { ?>
                    <div class="box box-danger">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?php echo $this->session->flashdata('error') ?></h3>
                        </div>
                    </div>
                <?php } ?>

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Conversation List</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <a href="<?php echo base_url(); ?>usadmin/conversation/add" class="btn btn-primary pull-left">Add New</a>
                        <br /><br />
                        <table id="categories-table" class="table table-bordered table-striped tree">
                            <thead>
                                <tr>
                                    <th>Age Group</th>
                                    <th>Section</th>
                                    <th>Category</th>
                                    <th>SubCategory</th>
                                    <th>Content Name</th>
                                    <th>Conversation Name</th>                                    
                                    <th>Parent</th>
                                    <th>Sort Order</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    foreach ($contents as $content) :
                                    $CI =& get_instance();
                                    $this->db->select('t1.content_name')->from('conversations as t1')->where('t1.conversation_id',$content['parent_id']);
                                    $query = $this->db->get();
                                    $result = $query->row_array();
                                    $parent_name = 'Top';
                                    if(count($result)>0) {
                                        $parent_name = $result['content_name'];
                                    }
                                    $this->db->select('age_group_name');
                                    $this->db->where('age_group_id',$content['age_group_id']);                                    
                                    $agegroup_val = $this->db->get('age_group')->row_array();
                                    
                                    $this->db->select('content_name');
                                    $this->db->where('content_id',$content['content_id']);
                                    $content_val = $this->db->get('contents')->row_array();
                                    //print_r($content); exit;
                               ?>
                                <tr>
                                    <td><?php echo $agegroup_val['age_group_name']?></td>
                                    <td><?php echo $content['section_name']?></td>
                                    <td><?php echo $content['category_name']?></td>
                                    <td><?php echo $content['subcategory_name']?></td>
                                    <td><?php echo $content_val['content_name']?></td>
                                    <td><?php echo $content['content_name']?></td>                                    
                                    <td><?php echo $parent_name?></td>
                                    <td><?php echo $content['sort_order']?></td>
                                    <td><a href="<?php echo base_url().'usadmin/conversation/edit/'. $content['conversation_id']?>">Edit</a> | <a class="delete_content" href="<?php echo base_url().'usadmin/conversation/delete/'. $content['conversation_id']?>">Delete</a></td>
                                    
                                </tr>
                                    
                                <?php 
                                        endforeach;
                                    ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- /.content-wrapper -->
<div class="confirm-modal">
    <div class="modal modal-danger" id="confirm-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Warning!</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure want to delete this category?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline" data-dismiss="modal">No</button>
                    <button type="button" class="btn btn-outline" id="confirm-delete-category">Yes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</div>
<!-- /.confirm-modal -->

<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <img src="" id="category_image_pop" class="img-responsive">
            </div>
        </div>
    </div>
</div>


<!-- DataTables -->
<script src="<?php echo base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/treeview/jquery.treegrid.min.js"></script>

<script>
    $(function () {

        $('#categories-table').DataTable({
            "paging": true,
            "deferRender": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });

        $('#categories-table1').DataTable({
            "paging": true,
            "deferRender": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
        $('.category_image').click(function () {
            // Get the modal
            $('#category_image_pop').attr('src', $(this).attr('image_url'));
        });

        $('a[id^="delete_category-"]').on('click', function () {
            var category_id = $(this).attr('data-id');
            $('#confirm-modal').modal('toggle');
            $('#confirm-delete-category').on('click', function () {

                $.ajax({
                    type: 'POST',
                    url: '<?php echo base_url(); ?>usadmin/category/delete',
                    dataType: 'html',
                    data: 'category_id=' + category_id,
                    success: function (result) {
                        if (result == 'success') {
                            $('#confirm-modal').modal('hide');
                            window.location.href = "<?php echo base_url(); ?>usadmin/category/";
                        } else {
                            $('#confirm-modal').modal('hide');
                            alert(result);
                        }
                    }
                });
            });
        });
        
       $('.delete_content').click(function(e){
       
        var r = confirm('Are you sure want to delete this Content?');
        
        if(r == false)
        {
        
        e.preventDefault();
        
        }    
        
        });

    });
</script>