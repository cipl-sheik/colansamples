<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Conversation
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="#"><i class="fa fa-dashboard"></i>Conversation</a></li>
            <li class="active">Edit Conversation</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">		  
            <div class="col-md-12">
                <?php if (validation_errors()) { ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                        <?php echo validation_errors(); ?>
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('Error')) { ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                        <h4>
                            <i class="icon fa fa-ban"></i>
                            Error!
                        </h4>
                        <?php echo $this->session->flashdata('Error'); ?>
                    </div>
                <?php } ?>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Conversation</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" name="section_form" action="<?php echo base_url(); ?>usadmin/conversation/edit/<?php echo $content[0]['conversation_id']?>" method="post" enctype="multipart/form-data">
                        <div class="box-body">
                            <?php// print_r($content); exit; ?>
                            
                            <div class="form-group col-xs-9">
                                <label>Age Group</label>
                                <select class="form-control" id="age_group_id" name="age_group_id">
                                    <option value="0">Select Age Group</option>
                                    <?php foreach ($age_groups as $age_group): ?>
                                        <option value="<?php echo $age_group['age_group_id']; ?>" <?php if($content[0]['age_group_id'] == $age_group['age_group_id']) echo 'selected'?>><?php echo $age_group['age_group_name']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            
                            <div class="form-group col-xs-9">
                                <label>Section</label>
                                <select class="form-control" id="section_id" name="section_id">
                                    <option value="0">Select Section</option>
                                    <?php foreach ($sections as $section): ?>
                                        <option value="<?php echo $section['section_id']; ?>" <?php if($content[0]['section_id'] == $section['section_id']) echo 'selected'?>><?php echo $section['section_name']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group col-xs-9">
                                <label>Category</label>
                                <select class="form-control" id="category_id" name="category_id">
                                    <option value="0">Select Category</option>
                                    <?php foreach ($content_cats as $content_cat): ?>
                                        <option value="<?php echo $content_cat['category_id']?>" <?php if($content[0]['category_id'] == $content_cat['category_id']) echo 'selected'?>><?php echo $content_cat['category_name']?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group col-xs-9">
                                <label>Sub Category</label>
                                <select class="form-control" id="subcategory_id" name="subcategory_id">
                                    <option value="0">Select Sub Category</option>
                                    <?php foreach ($subcategory as $subcontent_cat): ?>
                                        <option value="<?php echo $subcontent_cat['category_id']?>" <?php if($content[0]['subcategory_id'] == $subcontent_cat['category_id']) echo 'selected'?>><?php echo $subcontent_cat['category_name']?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group col-xs-9">
                                <label for="content_name">Content Name</label>
                                <input type="text" class="form-control" id="content_name" name="content_name" placeholder="Enter Content Name" value="<?php echo $content[0]['content_name'];?>">
                            </div>
                            
                            <input type="hidden" name="content_type" id="content_type" value="conversation"/>

                            <div class="form-group col-xs-9">
                                <label>Parent Content</label>
                                <select class="form-control" name="content_id" id="content_id">
                                    <option value="0">Top</option>
                                    <?php foreach ($root_contents as $root_content): ?>
                                        <option value="<?php echo $root_content['content_id']; ?>" <?php if($content[0]['content_id'] == $root_content['content_id']) { ?> selected="selected" <?php } ?>><?php echo $root_content['content_name'];?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>

                            <div class="form-group col-xs-9">
                                <label>Parent Conversation</label>
                                <select class="form-control" name="parent_id" id="parent_id">
                                    <option value="0">Top</option>
                                    <?php foreach ($root_conversations as $root_conversation): ?>
                                        <option value="<?php echo $root_conversation['conversation_id']; ?>" <?php if($content[0]['parent_id'] == $root_conversation['conversation_id']) { ?> selected="selected" <?php } ?>><?php echo $root_conversation['content_name'];?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>


                            <div class="form-group col-xs-9 book_sec" style="display:none;">
                                <label>Content</label>
                                <textarea class="form-control" rows="3" placeholder="Enter Content.." name="content"><?php echo $content[0]['content']?></textarea>
                            </div>
                            
                            <div class="form-group col-xs-9">
                                <input type="hidden" name="audio_url_val" value="<?php echo $content[0]['audio_url']?>"/>
                                <label>Audio Link</label>
                                <div><a href="<?php echo base_url().$content[0]['audio_url']?>" target="_blank">Audio File</a></div>
                            </div>

                            <div class="form-group col-xs-9 audio_sec">
                                <label>Audio</label>
                                <input type="file" name="audio_book" id="audio_book">
                            </div>

                            <div class="form-group col-xs-9">
                                <label for="sort_order">Sort Order</label>
                                <input type="text" class="form-control" id="sort_order" name="sort_order"  value="<?php echo $content[0]['sort_order'] ?>" placeholder="Enter Sort Order">
                            </div>
								<div class="form-group col-xs-9">
                  
                                <label>Paid Content</label>
                                <select class="form-control" name="need_payment">
                                    <option value="true"<?php echo ($content[0]["need_payment"] === 'true') ? ' selected="selected"':'';?>>Yes</option>
                                    <option value="false"<?php echo ($content[0]["need_payment"] === 'false') ? ' selected="selected"':'';?>>No</option>
                                </select>
                            </div>

                            <div class="form-group col-xs-6 content_sec">
                                <label for="background_image">Background Image (Common)-(1920 x 1080)</label><br />
                                <img width="200" src="<?php echo base_url() . $content[0]['background_image'];?>" />
                                <input type="file" name="background_image_conversation" id="background_image_conversation">
                            </div>

                            <div class="form-group col-xs-6 content_sec_i">
                                <label for="background_image_ipod_conversation">Background Image (I-Pod)-(2732 x 2048)</label><br />
                                <img width="200" src="<?php echo base_url() . $content[0]['background_image_mobile'];?>" />
                                <input type="file" name="background_image_ipod_conversation" id="background_image_ipod_conversation">
                            </div>


                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" id="section_submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>		  
                <!-- /.box -->
            </div>
            <!-- /.col -->		
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script type="text/javascript">
    $(document).ready(function() {

        $('#section_id').change(function() {

            section_id = $(this).val();
            age_id = $("#age_group_id").val();
            $.ajax({
                url: '<?php echo base_url() ?>usadmin/content/get_categories_ajax/',
                type: 'POST',
                dataType: 'json',
                data: {section_id: section_id},
                success: function(resp) {

                    cathtml = '<option value="0">Select Category</option>';
                    for (var i = 0; i < resp.categories.length; i++) {
                        category_id = resp.categories[i].category_id;
                        category_name = resp.categories[i].category_name;

                        cathtml += '<option value="' + category_id + '">' + category_name + '</option>';

                    }

                    $('#category_id').html(cathtml);
                    $('#category_id').val(0);
                    $('#subcategory_id').val(0);
                    $('#content_id').val(0);
                    $('#conversation_id').val(0);

                },
                error: function(req, status, err) {
                    alert("Something went Wrong. Pleaes try again");
                }
            });
            
            
            $.ajax({
                url: '<?php echo base_url() ?>usadmin/conversation/get_rootcontents_ajax/',
                type: 'POST',
                dataType: 'json',
                data: {age_id : age_id, section_id: section_id},
                success: function(resp) {

                    cathtml = '<option value="0">Top</option>';
                    for (var i = 0; i < resp.rootcontents.length; i++) {
                        content_id = resp.rootcontents[i].conversation_id;
                        content_name = resp.rootcontents[i].content_name;

                        cathtml += '<option value="' + content_id + '">' + content_name + '</option>';

                    }

                    $('#parent_id').html(cathtml);

                },
                error: function(req, status, err) {
                    alert("Something went Wrong. Pleaes try again");
                }
            });
            

        });

//        $('#content_type').change(function() {
//
//            $('#content_sec').hide();
//            $('#video_sec').hide();
//            $('.book_sec').hide();
//            $('.audio_sec').hide();
//
//            if ($(this).val() == 'text')
//                $('#content_sec').show();
//
//            if ($(this).val() == 'video')
//                $('#video_sec').show();
//
//            if ($(this).val() == 'book') {
//                $('.audio_sec').show();
//            }
//
//            if ($(this).val() == 'conversation') {
//                $('.book_sec').show();
//            }
//
//
//        });

        $('#age_group_id').change(function() {

            age_id = $(this).val();

            //console.log(section_id+"="+category_id);
            
            $.ajax({
                url: '<?php echo base_url() ?>usadmin/conversation/get_rootcontents_ajax/',
                type: 'POST',
                dataType: 'json',
                data: {age_id : age_id},
                success: function(resp) {
                    //console.log(resp); return;
                    cathtml = '<option value="0">Top</option>';
                    for (var i = 0; i < resp.rootcontents.length; i++) {
                        content_id = resp.rootcontents[i].conversation_id;
                        content_name = resp.rootcontents[i].content_name;

                        cathtml += '<option value="' + content_id + '">' + content_name + '</option>';

                    }

                    $('#parent_id').html(cathtml);
                    $('#section_id').val(0);
                    $('#category_id').val(0);
                    $('#subcategory_id').val(0);
                    $('#content_id').val(0);
                    $('#conversation_id').val(0);

                },
                error: function(req, status, err) {
                    alert("Something went Wrong. Pleaes try again");
                }
            });

        });

        $('#category_id').change(function() {

            section_id = $("#section_id").val();
            category_id = $("#category_id").val();
            age_id = $("#age_group_id").val();
            
            $.ajax({
                url: '<?php echo base_url() ?>usadmin/content/get_subcategories_ajax/',
                type: 'POST',
                dataType: 'json',
                data: {category_id: category_id, age_id: age_id},
                success: function(resp) {

                    cathtml = '<option value="0">Select Sub Category</option>';
                    for (var i = 0; i < resp.categories.length; i++) {
                        category_id = resp.categories[i].category_id;
                        category_name = resp.categories[i].category_name;

                        cathtml += '<option value="' + category_id + '">' + category_name + '</option>';

                    }

                    $('#subcategory_id').html(cathtml);
                    $('#subcategory_id').val(0);
                    $('#content_id').val(0);
                    $('#conversation_id').val(0);

                    //$('#category_id').val(old_cat_id);
                    //$.LoadingOverlay("hide");
                },
                error: function(req, status, err) {
                    //$.LoadingOverlay("hide");
                    alert("Something went Wrong. Pleaes try again");
                }
            });

            //console.log(section_id+"="+category_id+"="+subcategory_id);
            $.ajax({
                url: '<?php echo base_url() ?>usadmin/content/get_rootcontents_ajax/',
                type: 'POST',
                dataType: 'json',
                data: {section_id: section_id, category_id: category_id},
                success: function(resp) {

                    cathtml = '<option value="0">Top</option>';
                    for (var i = 0; i < resp.rootcontents.length; i++) {
                        content_id = resp.rootcontents[i].content_id;
                        content_name = resp.rootcontents[i].content_name;

                        cathtml += '<option value="' + content_id + '">' + content_name + '</option>';

                    }

                    $('#content_id').html(cathtml);

                },
                error: function(req, status, err) {
                    alert("Something went Wrong. Pleaes try again");
                }
            });
            
            $.ajax({
                url: '<?php echo base_url() ?>usadmin/conversation/get_rootcontents_ajax/',
                type: 'POST',
                dataType: 'json',
                data: {age_id : age_id, section_id: section_id, category_id: category_id},
                success: function(resp) {

                    cathtml = '<option value="0">Top</option>';
                    for (var i = 0; i < resp.rootcontents.length; i++) {
                        content_id = resp.rootcontents[i].conversation_id;
                        content_name = resp.rootcontents[i].content_name;

                        cathtml += '<option value="' + content_id + '">' + content_name + '</option>';

                    }

                    $('#parent_id').html(cathtml);

                },
                error: function(req, status, err) {
                    alert("Something went Wrong. Pleaes try again");
                }
            });



        });


        $('#subcategory_id').change(function() {

            section_id = $("#section_id").val();
            category_id = $("#category_id").val();

            subcategory_id = $(this).val();

            //console.log(section_id + "=" + category_id + "=" + subcategory_id);
            $.ajax({
                url: '<?php echo base_url() ?>usadmin/content/get_rootcontents_ajax/',
                type: 'POST',
                dataType: 'json',
                data: {section_id: section_id, category_id: category_id, subcategory_id: subcategory_id},
                success: function(resp) {

                    cathtml = '<option value="0">Top</option>';
                    for (var i = 0; i < resp.rootcontents.length; i++) {
                        content_id = resp.rootcontents[i].content_id;
                        content_name = resp.rootcontents[i].content_name;

                        cathtml += '<option value="' + content_id + '">' + content_name + '</option>';

                    }

                    $('#content_id').html(cathtml);
                    $('#content_id').val(0);
                    $('#conversation_id').val(0);

                },
                error: function(req, status, err) {
                    alert("Something went Wrong. Pleaes try again");
                }
            });
            
            $.ajax({
                url: '<?php echo base_url() ?>usadmin/conversation/get_rootcontents_ajax/',
                type: 'POST',
                dataType: 'json',
                data: {age_id : age_id, section_id: section_id, category_id: category_id, subcategory_id: subcategory_id},
                success: function(resp) {

                    cathtml = '<option value=0"">Top</option>';
                    for (var i = 0; i < resp.rootcontents.length; i++) {
                        content_id = resp.rootcontents[i].conversation_id;
                        content_name = resp.rootcontents[i].content_name;

                        cathtml += '<option value="' + content_id + '">' + content_name + '</option>';

                    }

                    $('#parent_id').html(cathtml);

                },
                error: function(req, status, err) {
                    alert("Something went Wrong. Pleaes try again");
                }
            });



        });
        
        
        $('#content_id').change(function() {

            age_id = $("#age_group_id").val();
            section_id = $("#section_id").val();
            category_id = $("#category_id").val();
            subcategory_id = $("#subcategory_id").val();
            content_id = $("#content_id").val();

            //console.log(section_id + "=" + category_id + "=" + subcategory_id);
            $.ajax({
                url: '<?php echo base_url() ?>usadmin/conversation/get_rootcontents_ajax/',
                type: 'POST',
                dataType: 'json',
                data: {age_id : age_id, section_id: section_id, category_id: category_id, subcategory_id: subcategory_id, content_id: content_id},
                success: function(resp) {

                    cathtml = '<option value="0">Top</option>';
                    for (var i = 0; i < resp.rootcontents.length; i++) {
                        content_id = resp.rootcontents[i].conversation_id;
                        content_name = resp.rootcontents[i].content_name;

                        cathtml += '<option value="' + content_id + '">' + content_name + '</option>';

                    }

                    $('#parent_id').html(cathtml);
                    $('#conversation_id').val(0);

                },
                error: function(req, status, err) {
                    alert("Something went Wrong. Pleaes try again");
                }
            });
            



        });



    });

</script>
