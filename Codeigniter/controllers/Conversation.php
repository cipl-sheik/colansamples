<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Content Class
 *
 * @author Colan
 */
class Conversation extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->helper('url');
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('admin/categories_model');
        $this->load->model('admin/age_groups_model');
        $this->load->model('admin/content_model');
        $this->load->model('admin/conversation_model');

        cipl_admin_auth();
    }

    public function index() {
        
    }

    // List Section

    public function list_content() {

        $data['contents'] = $this->conversation_model->getContentList();

        $this->load->view('admin/common/header');
        $this->load->view('admin/conversation/list', $data);
        $this->load->view('admin/common/footer');
    }

    // Add Section 
    public function add_content() {
        
        if ($_SERVER['REQUEST_METHOD'] == 'POST') { //allow only the http method is POST
            $config = array(                
                array(
                    'field' => 'age_group_id',
                    'label' => 'Age Group',
                    'rules' => 'trim|required'
                ),
                array(
                    'field' => 'content_name',
                    'label' => 'Content Name',
                    'rules' => 'trim|required'
                )
            );
            
            $video_url = '';
            $audio_url = '';
            
            if($this->input->post('content_type') == 'conversation')
            $this->form_validation->set_rules('audio_book', 'Audio Content', 'callback_audio_upload');
            
            
            $this->form_validation->set_rules($config);
            
            if ($this->form_validation->run() != FALSE) {
                                
                if($_FILES['audio_book']['name'] != ""){
                  $audio_url = 'assets/conversationAudios/'.$this->upload_data['file']['file_name'];  
                }
                

                $slug_name = $this->create_unique_slug($this->input->post('content_name'),'conversations');
                
                $update_array = array(
                    "section_id"  => $this->input->post('section_id'),
                    "category_id"  => $this->input->post('category_id'),
                    "subcategory_id"  => $this->input->post('subcategory_id'),
                    "content_type"  => $this->input->post('content_type'),
                    "content_name"  => $this->input->post('content_name'),
                    "content_slug"  => $slug_name,
                    "parent_id"  => $this->input->post('parent_id'),
                    "content_id"  => $this->input->post('content_id'),
                    "content"  => $this->input->post('content'),
                    "need_payment"  => $this->input->post('need_payment'),
                    "audio_url" => $audio_url,
                    "sort_order"    => $this->input->post("sort_order"),
                    "age_group_id"    => $this->input->post("age_group_id"),
                    "created_date"    => date('Y-m-d H:i:s')                   
                );
                
                $background_image_conversation = '';
                $background_images_error = false;
                $background_images_error_messages = '';
                if ( isset($_FILES['background_image_conversation']) && $_FILES['background_image_conversation']["name"] != '' ) {
                    $upload_path = 'assets/conversationImages/';
                    $ext_image_name = explode(".", $_FILES['background_image_conversation']['name']);
                    $count_ext_img = count($ext_image_name);
                    $image_ext = $ext_image_name[$count_ext_img - 1];
                   // $file_name = base64_encode(trim($this->input->post('content_name'))) . '_' . md5(rand(1000000, 1000000000)) . '.' . $image_ext;
                    $file_name = $slug_name . '_' . md5(rand(1000000, 1000000000)) . '.' . $image_ext;
                    $file_name = str_replace('=', '', $file_name);
                    $image_info = getimagesize($_FILES["background_image_conversation"]["tmp_name"]);
                    $image_width = $image_info[0];
                    $image_height = $image_info[1];
                    $background_image = cipl_image_upload($_FILES['background_image_conversation'], 'background_image_conversation', $upload_path, $file_name, $image_width, $image_height);

                    if (isset($background_image['success'])) {
                        $background_image_conversation = $upload_path . $file_name;
                    } else {
                        $background_images_error = true;
                        $background_image_conversation = '';
                        $background_images_error_messages .= $background_image['messages']['error'];
                    }
                }
                
                $background_image_ipod_conversation = '';
                $background_images_ipod_error = false;
                $background_images_error_messages = '';
                if ( isset($_FILES['background_image_ipod_conversation']) && $_FILES['background_image_ipod_conversation']["name"] != '' ) {
                    $upload_path = 'assets/conversationImages/';
                    $ext_image_name = explode(".", $_FILES['background_image_ipod_conversation']['name']);
                    $count_ext_img = count($ext_image_name);
                    $image_ext = $ext_image_name[$count_ext_img - 1];
                    $file_name_ipod = $slug_name . '_ipod_' . md5(rand(1000000, 1000000000)) . '.' . $image_ext;
                    $file_name_ipod = str_replace('=', '', $file_name_ipod);
                    $image_info = getimagesize($_FILES["background_image_ipod_conversation"]["tmp_name"]);
                    $image_width = $image_info[0];
                    $image_height = $image_info[1];
                    $background_image = cipl_image_upload($_FILES['background_image_ipod_conversation'], 'background_image_ipod_conversation', $upload_path, $file_name_ipod, $image_width, $image_height);

                    if (isset($background_image['success'])) {
                        $background_image_ipod_conversation = $upload_path . $file_name_ipod;
                    } else {
                        $background_images_ipod_error = true;
                        $background_image_ipod_conversation = '';
                        $background_images_error_messages .= $background_image['messages']['error'];
                    }
                }

                if ($background_images_error == false) {
                    $background_image_result_array = array("background_image" => $background_image_conversation,"background_image_mobile" => $background_image_ipod_conversation);
                    $update_array = array_merge($background_image_result_array, $update_array);
                    $this->conversation_model->addContent($update_array);
                    $this->session->set_flashdata('Success', 'Conversation added successfully.');
                    redirect('usadmin/conversation', 'refresh');
                } else {
                    $this->session->set_flashdata('Error', $background_images_error_messages);
                }

            }
        }
        
        
        $data['templates'] = $this->getTemplateFiles();
        $data['sections'] = $this->categories_model->getAllSections();
        $data['age_groups'] = $this->age_groups_model->get_all_age_groups();
        $data['root_contents'] = $this->content_model->getRootContents();
        $data['root_conversations'] = $this->conversation_model->getRootContents();
        
        $this->load->view('admin/common/header');
        $this->load->view('admin/conversation/add',$data);
        $this->load->view('admin/common/footer');
    }
    
     public function edit_content($content_id) {
        
        $data['content'] = $this->conversation_model->getContentById($content_id); 
        if ($_SERVER['REQUEST_METHOD'] == 'POST') { //allow only the http method is POST
            $config = array(
                array(
                    'field' => 'age_group_id',
                    'label' => 'Age Group',
                    'rules' => 'trim|required'
                ),
                array(
                    'field' => 'content_name',
                    'label' => 'Content Name',
                    'rules' => 'trim|required'
                )
            );
            
            
            $audio_url = $this->input->post('audio_url_val');   
            $content = $this->input->post('content');
            
            if($this->input->post('content_type') == 'conversation')
            {
                if($_FILES['audio_book']['name'] != ""){
                    $this->form_validation->set_rules('audio_book', 'Audio Content', 'callback_audio_upload');
                }
            }
            
            
            $this->form_validation->set_rules($config);
            
            if ($this->form_validation->run() != FALSE) {                
              
                                
                if($_FILES['audio_book']['name'] != ""){
                    $audio_url = 'assets/conversationAudios/'.$this->upload_data['file']['file_name'];  
                }  else {
                    $audio_url = $this->input->post('audio_url_val');   
                }
                
                $slug_name = $this->create_unique_slug($this->input->post('content_name'),'conversations','content_slug','conversation_id',$content_id);
                
                $update_array = array(
                    "section_id"  => $this->input->post('section_id'),
                    "category_id"  => $this->input->post('category_id'),
                    "subcategory_id"  => $this->input->post('subcategory_id'),
                    "content_type"  => $this->input->post('content_type'),
                    "content_name"  => $this->input->post('content_name'),
                    "content_slug"  => $slug_name,
                    "parent_id"  => $this->input->post('parent_id'),
                    "content_id"  => $this->input->post('content_id'),
					"need_payment"  => $this->input->post('need_payment'),
                    "content"  => $this->input->post('content'),
                    "audio_url" => $audio_url,
                    "sort_order"    => $this->input->post("sort_order"),
                    "age_group_id"    => $this->input->post("age_group_id"),
                    "created_date"    => date('Y-m-d H:i:s')                   
                );

                $background_image_conversation = $data['content'][0]['background_image'];
                $background_images_error = false;
                $background_images_error_messages = '';
                if ( isset($_FILES['background_image_conversation']) && $_FILES['background_image_conversation']["name"] != '' ) {
                    $upload_path = 'assets/conversationImages/';
                    $ext_image_name = explode(".", $_FILES['background_image_conversation']['name']);
                    $count_ext_img = count($ext_image_name);
                    $image_ext = $ext_image_name[$count_ext_img - 1];
                   // $file_name = base64_encode(trim($this->input->post('content_name'))) . '_' . md5(rand(1000000, 1000000000)) . '.' . $image_ext;
                    $file_name = $slug_name . '_' . md5(rand(1000000, 1000000000)) . '.' . $image_ext;
                    $file_name = str_replace('=', '', $file_name);
                    $image_info = getimagesize($_FILES["background_image_conversation"]["tmp_name"]);
                    $image_width = $image_info[0];
                    $image_height = $image_info[1];
                    $background_image = cipl_image_upload($_FILES['background_image_conversation'], 'background_image_conversation', $upload_path, $file_name, $image_width, $image_height);

                    if (isset($background_image['success'])) {
                        $background_image_conversation = $upload_path . $file_name;
                    } else {
                        $background_images_error = true;
                        $background_image_conversation = '';
                        $background_images_error_messages .= $background_image['messages']['error'];
                    }
                }
                
                $background_image_ipod_conversation = $data['content'][0]['background_image_mobile'];
                $background_images_ipod_error = false;
                $background_images_error_messages = '';
                if ( isset($_FILES['background_image_ipod_conversation']) && $_FILES['background_image_ipod_conversation']["name"] != '' ) {
                    $upload_path = 'assets/conversationImages/';
                    $ext_image_name = explode(".", $_FILES['background_image_ipod_conversation']['name']);
                    $count_ext_img = count($ext_image_name);
                    $image_ext = $ext_image_name[$count_ext_img - 1];
                    $file_name_ipod = $slug_name . '_ipod_' . md5(rand(1000000, 1000000000)) . '.' . $image_ext;
                    $file_name_ipod = str_replace('=', '', $file_name_ipod);
                    $image_info = getimagesize($_FILES["background_image_ipod_conversation"]["tmp_name"]);
                    $image_width = $image_info[0];
                    $image_height = $image_info[1];
                    $background_image = cipl_image_upload($_FILES['background_image_ipod_conversation'], 'background_image_ipod_conversation', $upload_path, $file_name_ipod, $image_width, $image_height);

                    if (isset($background_image['success'])) {
                        $background_image_ipod_conversation = $upload_path . $file_name_ipod;
                    } else {
                        $background_images_ipod_error = true;
                        $background_image_ipod_conversation = '';
                        $background_images_error_messages .= $background_image['messages']['error'];
                    }
                }
                
                if ($background_images_error == false) {
                    $background_image_result_array = array("background_image" => $background_image_conversation,"background_image_mobile" => $background_image_ipod_conversation);
                    $update_array = array_merge($background_image_result_array, $update_array);
                    $this->conversation_model->updateContent($update_array, $content_id);
                    $this->session->set_flashdata('Success', 'Content updated successfully.');
                    redirect('usadmin/conversation', 'refresh');
                } else {
                    $this->session->set_flashdata('Error', $background_images_error_messages);
                }
                
                 /*$this->conversation_model->updateContent($update_array,$content_id);
                 $this->session->set_flashdata('Success', 'Content updated successfully.');
                 redirect('usadmin/content', 'refresh');*/
         
            }
        }
        
        $data['templates'] = $this->getTemplateFiles();
        $data['sections'] = $this->categories_model->getAllSections();
        $data['age_groups'] = $this->age_groups_model->get_all_age_groups();        
        $data['content_cats'] = $this->categories_model->getCategoriesbySection($data['content'][0]['section_id']);
        $data['root_contents'] = $this->content_model->getRootContents($data['content'][0]['section_id'],$data['content'][0]['category_id'],$data['content'][0]['subcategory_id']);
        $data['subcategoryinfo'] = $this->categories_model->getCategory($data['content'][0]['subcategory_id']);
        $data['subcategory'] = $this->categories_model->getSubCategoriesbySection($data['content'][0]['category_id'],$data['subcategoryinfo'][0]['category_age_group_id']);        
        $data['root_conversations'] = $this->conversation_model->getRootContents($data['content'][0]['age_group_id'],$data['content'][0]['section_id'],$data['content'][0]['category_id'],$data['content'][0]['subcategory_id'],$data['content'][0]['content_id']);

        $this->load->view('admin/common/header');
        $this->load->view('admin/conversation/edit',$data);
        $this->load->view('admin/common/footer');
    }

  
    
    public function get_categories_ajax()
    {
        
        $section_id = $this->input->post('section_id');
        $cats['categories'] = $this->categories_model->getCategoriesbySection($section_id);
        echo json_encode($cats);
        
    }

    public function get_subcategories_ajax()
    {

        $category_id = $this->input->post('category_id');
        $age_id = '0';
        if(isset($_REQUEST['age_id']))
        {
            $age_id = $this->input->post('age_id');
        }
        $cats['categories'] = $this->categories_model->getSubCategoriesbySection($category_id,$age_id);
        echo json_encode($cats);

    }
    
    public function delete_content($content_id)
    {
        $this->conversation_model->deleteContent($content_id);
        $this->session->set_flashdata('Success', 'Conversation deleted successfully.');
        redirect('usadmin/conversation', 'refresh');
        
    }
    
    
    function video_upload(){
	  if($_FILES['video_file']['size'] != 0){
		$upload_dir = './assets/contentVideos';
		if (!is_dir($upload_dir)) {
		     mkdir($upload_dir);
		}	
		$config['upload_path']   = $upload_dir;
		$config['allowed_types'] = 'mp4';
		$config['file_name']     = 'content_vid_'.time().md5(rand());
		$config['overwrite']     = true;
		

		$this->load->library('upload', $config);
		if (!$this->upload->do_upload('video_file')){
			$this->form_validation->set_message('video_upload', $this->upload->display_errors());
			return false;
		}	
		else{
			$this->upload_data['file'] =  $this->upload->data();
			return true;
		}	
	}	
	else{
		$this->form_validation->set_message('video_upload', "No file selected");
		return false;
	}
    }
    
    
    function audio_upload(){
	  if($_FILES['audio_book']['size'] != 0){
		$upload_dir = './assets/conversationAudios';
		if (!is_dir($upload_dir)) {
		     mkdir($upload_dir);
		}	
		$config['upload_path']   = $upload_dir;
		$config['allowed_types'] = 'mp3|wav|mpeg';
		$config['file_name']     = 'conversation_aud_'.time().md5(rand());
		$config['overwrite']     = true;
		

		$this->load->library('upload', $config);
		if (!$this->upload->do_upload('audio_book')){
			$this->form_validation->set_message('audio_book', $this->upload->display_errors());
			return false;
		}	
		else{
			$this->upload_data['file'] =  $this->upload->data();
			return true;
		}	
	}	
//	else{
//		$this->form_validation->set_message('audio_upload', "No file selected");
//		return false;
//	}
    }
    
    
    public function getTemplateFiles()
    {
        
        $files = glob('./application/views/front/content-templates/*.php');
        
        $templates = array();
        $indi = 0;
        foreach($files as $fname) 
        {
        $indi++;
        preg_match( '|Template Name:(.*)$|mi', file_get_contents( $fname ), $header );
        
        $templates[$indi]['temp_path'] = $fname;
        $templates[$indi]['temp_name'] = $header[1];
        }
        
        return $templates;
    }

    //Create Unique Slug
    function create_unique_slug($string,$table,$field='content_slug',$key=NULL,$value=NULL)
    {
        $t =& get_instance();
        $slug = url_title($string);
        $slug = strtolower($slug);
        $i = 0;
        $params = array ();
        $params[$field] = $slug;

        if($key)$params["$key !="] = $value;

        while ($t->db->where($params)->get($table)->num_rows())
        {
            if (!preg_match ('/-{1}[0-9]+$/', $slug ))
                $slug .= '-' . ++$i;
            else
                $slug = preg_replace ('/[0-9]+$/', ++$i, $slug );

            $params [$field] = $slug;
        }
        return $slug;
    }

    public function get_rootcontents_ajax()
    {
        $age_id = '0';
        if(isset($_REQUEST['age_id']))
        {
            $age_id = $this->input->post('age_id');
        }        
        
        $section_id = '0';
        if(isset($_REQUEST['section_id']))
        {
            $section_id = $this->input->post('section_id');
        }
        
        $category_id = '0';
        if(isset($_REQUEST['category_id']))
        {
            $category_id = $this->input->post('category_id');
        }
        
        $subcategory_id = '0';
        if(isset($_REQUEST['subcategory_id']))
        {
            $subcategory_id = $this->input->post('subcategory_id');
        }
        
        $content_id = '0';
        if(isset($_REQUEST['content_id']))
        {
            $content_id = $this->input->post('content_id');
        }
        
        //echo $content_id; exit;
        
        $cats['rootcontents'] = $this->conversation_model->getRootContentsbySection($age_id,$section_id,$category_id,$subcategory_id,$content_id);
        echo json_encode($cats);

    }

}