<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *  Content Model Class
 *
 *  @version         1.0
 *  @author          Colan
 */
class Conversation_model extends CI_Model {

    /**
     * Constructor
     * 
     * @access public
     */
    protected $content_tbl;
  

    function __construct() {
        parent::__construct();
        $this->content_tbl = "conversations";
        
    }

    

    public function addContent($data) {
        return $this->db->insert($this->content_tbl, $data);
    }
    
    public function updateContent($data,$content_id) {
        
        $this->db->where('conversation_id', $content_id);
        return $this->db->update($this->content_tbl, $data); 
        
    }
    
     public function getContentList() {
        $this->db->select('t1.content_type,t1.conversation_id,t1.subcategory_id,t1.content_name,t1.parent_id,t1.created_date,t1.sort_order,t2.section_name,t3.category_name,t1.age_group_id,t1.content_id')->from('conversations as t1');
        $this->db->join('sections as t2','t2.section_id = t1.section_id', 'left outer');
        $this->db->join('categories as t3','t3.category_id = t1.category_id', 'left outer');
        $this->db->order_by('t1.age_group_id,t2.section_id,t3.category_id,t1.content_id,t1.sort_order', 'ASC');
        $query = $this->db->get();
         $result = $query->result_array();
         //print_r($result); exit;
		foreach($result as $data){
			$response[]=array(
			'section_name'=>$data['section_name'],
			'parent_id'=>$data['parent_id'],
			'category_name'=>$data['category_name'],
			'content_name'=>$data['content_name'],
			'content_type'=>$data['content_type'],
			'sort_order'=>$data['sort_order'],
			'created_date'=>$data['created_date'],
			'conversation_id'=>$data['conversation_id'],
                        'content_id'=>$data['content_id'],    
                        'age_group_id'=>$data['age_group_id'],    
			'subcategory_name'=>$this->getContentSubCategoryId($data['subcategory_id'])
			);
			
		}
		return $response;
		
    }
    public function getContentSubCategoryId($subcategory_id)
    { 
       	$this->db->where('category_id',$subcategory_id);
        $query= $this->db->get('categories')->row();
        if(count($query)>0)
        {
            return $query->category_name;
        }
        else
        {
            return '';
        }
        
    }
    
    public function getContentById($content_id)
    {
        
        $this->db->select('t1.*,t2.category_name')->from('conversations as t1')->where('t1.conversation_id',$content_id);
        $this->db->join('categories as t2','t2.category_id = t1.category_id', 'left outer');
        $query = $this->db->get();
        return $result = $query->result_array();
        
        
    }
    
    public function deleteContent($content_id) {
        
        $result = $this->db->delete($this->content_tbl, array('conversation_id' => $content_id));
        return $result;
    }

    public function getRootContents($age_id = 0, $section_id = 0,$category_id = 0,$subcategory_id = 0,$content_id = 0) {
      $this->db->select('conversation_id,content_name')
        ->from($this->content_tbl)
        ->where('parent_id', '0')
        ->where('section_id', $section_id)
        ->where('category_id', $category_id)
        ->where('age_group_id', $age_id)      
	->where('subcategory_id', $subcategory_id)
        ->where('content_id', $content_id)      
        ->order_by('content_name', 'ASC');
      $query = $this->db->get();
      return $root_categories = $query->result_array();
    }

    public function getRootContentsbySection($age_id,$section_id = 0,$category_id = 0,$subcategory_id = 0,$content_id = 0)
    {

      $this->db->select('conversation_id,content_name')
        ->from($this->content_tbl)
        ->where('parent_id', '0')
        ->where('section_id', $section_id)
        ->where('category_id', $category_id)
        ->where('subcategory_id', $subcategory_id)
        ->where('age_group_id', $age_id)
        ->where('content_id', $content_id)      
        ->order_by('content_name', 'ASC');

      $query = $this->db->get();
      $root_categories = $query->result_array();
      return $root_categories;


    }

   

}
