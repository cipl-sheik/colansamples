<?php

namespace App\Http\Requests\Account;

use Illuminate\Foundation\Http\FormRequest;

class AccountEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'account_type_id'   => 'required|exists:account_types,id',
            'number'            => 'nullable|min:5|unique:accounts,number,'.$this->route('account'),
            'account_name'      => 'required|string',
            'description'       => 'nullable|string',
            'bank_account_no'   => 'nullable',
            'status'            => 'nullable|boolean',
        ];
    }
}
