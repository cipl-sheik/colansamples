<?php

namespace App\Traits;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

trait ApiResponse
{
    protected $errors  = 'errors';
    protected $data    = 'data';

    public function gate($permission)
    {
        if (! Gate::allows($permission)) {
            return abort(401);
        }
        return null;
    }

    public function dataSuccess($resource, $code = 200)
    {
        return $this->_response($resource, $code);
    }

    public function dataErrorResponse($message, $code = 401)
    {
        $error = ['message' => $message];

        return response()->json([
            $this->errors   => $error
        ], $code);
    }

    public function handleException($request, \Exception $exception)
    {
        if ($exception instanceof ValidationException) {
            return $this->dataValidationException($exception);
        }
        if ($exception instanceof ModelNotFoundException) {
            $modelName = strtolower(class_basename($exception->getModel()));
            return $this->dataErrorResponse("Does not exists any {$modelName} with the specified id", 404);
        }
        if ($exception instanceof AuthenticationException) {
            return $this->dataErrorResponse('Unauthenticated', 401);
        }
        if ($exception instanceof AuthorizationException) {
            return $this->dataErrorResponse($exception->getMessage(), 403);
        }
        if ($exception instanceof MethodNotAllowedHttpException) {
            return $this->dataErrorResponse('The specified method for the request is invalid', 405);
        }
        if ($exception instanceof NotFoundHttpException) {
            return $this->dataErrorResponse('The specified URL cannot be found', 404);
        }
        if ($exception instanceof HttpException) {
            return $this->dataErrorResponse($exception->getMessage(), $exception->getStatusCode());
        }
        if ($exception instanceof QueryException) {
            $errorCode = $exception->errorInfo[1];
            if ($errorCode == 1451) {
                return $this->dataErrorResponse('Cannot remove this resource permanently. It is related with any other resource', 409);
            }
        }
        if ($exception instanceof TokenMismatchException) {
            return redirect()->back()->withInput($request->input());
        }

        return $this->dataErrorResponse($exception->getMessage(), 500);
    }

    private function dataValidationException(\Exception $exception)
    {
        $errors = $exception->errors();
        return response()->json([
            $this->errors => $errors
            ,
        ], 401);
    }

    private function _response($resource, $code)
    {
        return response()->json($resource, $code);
    }
}