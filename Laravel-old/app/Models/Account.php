<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Account extends Model
{
    use LogsActivity;
    const PERMISSIONS = 'manage_accounts';

    protected $fillable = [
        'account_type_id',
        'number',
        'account_name',
        'description',
        'bank_account_no',
        'parent_id',
        'status',
    ];

    protected $appends = ['account_type_name'];

    protected static $logFillable = true;

    public function scopeParent($query)
    {
        return $query->where('parent_id', null);
    }

    public function scopeChild($query)
    {
        return $query->where('parent_id', '!=', null);
    }

    public function children()
    {
        return $this->hasMany(self::class, 'parent_id')->with('children');
    }

    public function parents()
    {
        return $this->belongsTo(self::class, 'parent_id');
    }

    public function accountType()
    {
        return $this->belongsTo(AccountType::class);
    }

    public function getAccountTypeNameAttribute()
    {
        return $this->accountType()->value('slug');
    }
}
