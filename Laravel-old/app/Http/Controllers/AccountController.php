<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\Account\AccountAddRequest;
use App\Http\Requests\Account\AccountEditRequest;
use App\Models\Account;
use App\Models\AccountType;
use App\Traits\ApiResponse;

class AccountController extends Controller
{
    use ApiResponse;

    public function index()
    {
        $this->gate(Account::PERMISSIONS);
        $pageName = 'Accounts';
        $accountType = AccountType::all();
        return view('sales.account.index', compact('pageName', 'accountType'));
    }

    public function showAll()
    {
        $accounts = Account::with('accountType')->latest()
           ->paginate(request('per_page') ?? 15);
        return $this->dataSuccess($accounts);
    }

    public function store(AccountAddRequest $request)
    {   $this->gate(Account::PERMISSIONS);
        $account = Account::create($request->all());
        return $this->dataSuccess($account);
    }

    public function update(AccountEditRequest $request, $id)
    {   $this->gate(Account::PERMISSIONS);
        $account = Account::findOrFail($id)->update($request->all());
        return $this->dataSuccess($account);
    }

    public function show($id)
    {
        $account = Account::with(['accountType', 'parents'])->findOrFail($id);
        return $this->dataSuccess($account);
    }

    public function delete($id)
    {   $this->gate(Account::PERMISSIONS);
        $account = Account::findOrFail($id);
        return $this->dataSuccess($account->delete());
    }

    public function fetchAll()
    {
        $accounts = Account::with('accountType')
            ->when(request()->has('id'), function ($query) {
                $query->where('id', '!=', request('id'));
            })->get();
        return $this->dataSuccess($accounts);
    }

    public function status($id)
    {   $this->gate(Account::PERMISSIONS);
        $status = (request('status') == 0) ? 1 : 0;

        $account = Account::findOrFail($id);
        $account->update(['status'=>$status]);

        return $this->dataSuccess($account);
    }

}
