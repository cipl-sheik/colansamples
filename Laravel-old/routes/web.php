<?php

Route::middleware(['auth'])->group(function () {
    // Account
    Route::get('/accounts', 'AccountController@index')->name('sales.account.index');
    Route::get('/accounts/show-all', 'AccountController@showAll')->name('sales.account.showAll');
    Route::get('/accounts/fetch-all', 'AccountController@fetchAll')->name('sales.account.fetchAll');
    Route::post('/accounts', 'AccountController@store')->name('sales.account.add');
    Route::get('/accounts/{account?}', 'AccountController@show')->name('sales.account.show');
    Route::put('/accounts/{account?}', 'AccountController@update')->name('sales.account.update');
    Route::delete('/accounts/{account?}', 'AccountController@delete')->name('sales.account.delete');
    Route::put('accounts/status/{id?}', 'AccountController@status')->name('sales.account.status');
});