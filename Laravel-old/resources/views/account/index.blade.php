@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <account-list
                    page-name="{{ __('Account List') }}"
                    account-api-url="{{ route('sales.account.showAll') }}"
                    account-fetch-all-url="{{ route('sales.account.fetchAll') }}"
                    account-store-url = "{{ route('sales.account.add') }}"
                    account-show-url = "{{ route('sales.account.show') }}"
                    account-update-url = "{{ route('sales.account.update') }}"
                    account-delete-url = "{{ route('sales.account.delete') }}"
                    account-status-url ="{{ route('sales.account.status') }}"
					get-accounttype-url = "{{$accountType}}"
            ></account-list>
        </div>
    </div>
@endsection