<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SquadController;
use App\Http\Controllers\PositionController;
use App\Http\Controllers\EmployeeController;

Route::get('/', function () { return view('welcome'); });

Route::get('api/v1/squads', [SquadController::class, 'index']);
Route::post('api/v1/squads', [SquadController::class, 'save']);
Route::put('api/v1/squads/{id}', [SquadController::class, 'update']);
Route::delete('api/v1/squads/{id}', [SquadController::class, 'delete']);


Route::get('api/v1/positions', [PositionController::class, 'index']);
Route::post('api/v1/positions', [PositionController::class, 'save']);
Route::put('api/v1/positions/{id}', [PositionController::class, 'update']);
Route::delete('api/v1/positions/{id}', [PositionController::class, 'delete']);


Route::get('api/v1/employees', [EmployeeController::class, 'index']);
Route::post('api/v1/employees', [EmployeeController::class, 'save']);
Route::put('api/v1/employees/{id}', [EmployeeController::class, 'update']);
Route::delete('api/v1/employees/{id}', [EmployeeController::class, 'delete']);









